
# Configuring Services

1. Configuring Promtail for incomming Messages
2. Configuring Output Sources (Syslog as Example)

# Configuring Promtail

Copy your local file into the Container:

* `docker cp config.yml lam-promtail:/etc/promtail/config.yml`

Apply Configuration after changes with restarting the promtail service:

* `docker restart lam-promtail`

Btw: Switch the parameters to copy from the container:

* `docker cp lam-promtail:/etc/promtail/config.yml config.yml`


```yml
# config.yml

server:
  http_listen_port: 9080
  grpc_listen_port: 0

positions:
  filename: /tmp/positions.yaml

clients:
  - url: http://loki:3100/loki/api/v1/push

scrape_configs:

  # Destination for forwarded Syslog Messages 
  - job_name: syslog
    syslog:
      listen_address: 0.0.0.0:9514
      labels:
        job: "syslog"
    relabel_configs:
      - source_labels: ['__syslog_message_hostname']
        target_label: 'host'
```
**Ressources:**

  * https://grafana.com/docs/loki/latest/clients/promtail/configuration/

## Configuring Syslog Outputs

**Syslog-NG Output Configuration:**

Copy the Configuration file from the container:

* `docker cp lam-syslog-ng:/etc/syslog-ng/syslog-ng.conf syslog-ng.conf` 

Alter and copy your local file back into the Container:

* `docker cp syslog-ng.conf lam-syslog-ng:/etc/syslog-ng/syslog-ng.conf`

Reload Configuration File without restart:

* `docker exec -it lam-syslog-ng syslog-ng-ctl reload`

**Add promtail as a additional destination for loki**
```conf
# syslog-ng.conf
...
destination d_loki {
  syslog("promtail" transport("tcp") port(9514));
};

...

log {
    ...
	destination(d_loki); # add promtail destination
};

```

**Syslog-ng logrotation**

* The syslog-ng server does not rotate logs by itself.
* Syslog-ng requires frequent restarts (reloads) when files are rotated.
* https://www.syslog-ng.com/technical-documents/doc/syslog-ng-open-source-edition/3.16/administration-guide/86


The lam-syslog-ng Service has already a propper logrotation Configuration:

`/etc/logrotate.d/syslog-ng`

**(Alternative) Rsyslog Output Configuration:**

```conf
action(type="omfwd" protocol="tcp" port="9514" Template="RSYSLOG_SyslogProtocol23Format" TCP_Framing="octet-counted")
```